%global alphatag 2008.6.1
Name:           xml2dict
Version:        0
Release:        0.3.%{alphatag}
Summary:        Use attributes of dictionary to access XML elements

Group:          Development/Libraries
License:        GPLv2+
URL:            http://code.google.com/p/xml2dict/
Source0:        http://xml2dict.googlecode.com/files/xml2dict-%{alphatag}-tar.gz

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools

%description
Use attributes of dictionary to access XML elements.

%prep
%setup -q -n %{name}-read-only

%build

%install
install -d $RPM_BUILD_ROOT%{python_sitelib}/
install -pm 0644 xml2dict.py $RPM_BUILD_ROOT%{python_sitelib}/
install -pm 0644 object_dict.py $RPM_BUILD_ROOT%{python_sitelib}/


%files
%doc README LICENSE
%{python_sitelib}/xml2dict.py*
%{python_sitelib}/object_dict.py*


%changelog
* Mon Mar 19 2012 Yuguang Wang <yuwang@redhat.com> - 0-0.3
- Update license files and fix install issues.

* Sun Jan 29 2012 Yuguang Wang <yuwang@redhat.com> - 0-0.2
- Fix koji build issues in spec.

* Thu Dec 22 2011 Yuguang Wang <yuwang@redhat.com> - 0-0.1
- Initial RPM release
