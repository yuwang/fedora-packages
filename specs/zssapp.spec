# Disable the brp-java-repack-jars
%define __os_install_post %{nil}
%define __jar_repack %{nil}

Name:           zssapp
Version:        2.1.0
Release:        1
Summary:        ZK Spreadsheet
Group:          Applications/Internet
License:        Internal RH for now
URL:            http://zkspreadsheet.googlecode.com/files/zss-app-2.1.0.zip
Source0:        %{name}-%{version}-%{release}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
prefix:         /var/lib/jbossas/server/production/deploy
# Requires:       httpd, mod_ssl, mod_auth_kerb, mysql-server,jbossas-4.3.0.GA_CP09-bin
%description
ZK Spreadsheet.

%prep
%setup -q -n %{name}-%{version}-%{release}

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -pv $RPM_BUILD_ROOT
cp -r * $RPM_BUILD_ROOT

%clean
rm -fr $RPM_BUILD_ROOT

%post
# Add the vault service to init.d
#/sbin/chkconfig --add vaultd

%files
%defattr(-,root,root,-)
/*

%changelog

* Wed Aug 31 2011 Yuguang Wang <yuwang@redhat.com>  - 2.1.0-1
- Initial build.

