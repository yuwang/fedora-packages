Name:           wadofstuff-django-serializers
Version:        1.1.0
Release:        1%{?dist}
Summary:        Extended Django Serializer Module

Group:          Development/Libraries
License:        BSD
URL:            http://code.google.com/p/wadofstuff/
Source0:        http://wadofstuff.googlecode.com/files/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools   

%description
Extended serializers for Django.

%prep
%setup -q -n %{name}-%{version}
sed -i "s|\r||g" PKG-INFO


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
%{__python} setup.py install --skip-build --root="$RPM_BUILD_ROOT"


%files
%doc AUTHORS ChangeLog INSTALL LICENSE PKG-INFO README
%{python_sitelib}/wadofstuff/
%{python_sitelib}/wadofstuff*.egg-info


%changelog
* Mon Aug 22 2011 Yuguang Wang <yuwang@redhat.com> - 1.1.0-1
- Add release number in version string

* Mon Aug 8 2011 Yuguang Wang <yuwang@redhat.com> - 1.1.0
- Initial RPM release
